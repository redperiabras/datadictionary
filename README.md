# Cata (RCBC Data Dictionary)

Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Tools

- [ ] [Docker](https://www.docker.com/products/docker-desktop/)
- [ ] [VSCode](https://code.visualstudio.com/download)
- [ ] [Git](https://git-scm.com/downloads)

## Project Setup

### 1. Install GIT

If you haven't already, make sure Git is installed on your machine. You can download it from the official website: <https://git-scm.com/downloads>

### 2. Open your terminal or command prompt

Open the terminal or command prompt on your computer. This step might vary depending on your operating system.

### 3. Navigate to the desired directory

Use the cd command to navigate to the directory where you want to clone the project. For example, if you want to clone it to your desktop, you can use:

```sh
cd Desktop
```

### 4. Clone the project

In the terminal or command prompt, use the git clone command followed by the URL of the GitLab project you want to clone. For example:

```sh
git clone <project_url>
```

Replace ```<project_url>``` with the actual URL of the project (<https://gitlab.com/rcbc-dsag-interns/rcbc-data-dictionary.git>).

### 5. Enter your GitLab credentials (if required)

If the project is private and requires authentication, you might be prompted to enter your GitLab username and password or provide an access token. Enter the required credentials to proceed.

### 6. Wait for the cloning process to complete

Git will clone the project's repository to your local machine. You'll see the progress and the files being downloaded. Once the process is complete, you'll have a local copy of the project.

### 7. Access the cloned project

Navigate to the directory where the project was cloned. Switch from main branch to develop.

```sh
git checkout develop
```

That's it! You have successfully cloned the project from GitLab. You can now work with the code and make changes as needed

## Intializing App

Once you properly installed Docker to your workstation, it should be easy to build and run this project on your own machine. This project template has built-in python, postgre database, and nginx server.

1. Open a terminal, and open the project folder.
2. To initialize the project, run the following command:
   
    ```sh 
    docker-compose build
    ```
3. Then, to run the app, run the following command:
    
    ```sh
    docker-compose up
    ```
4. Next, in a separate terminal, get the django-app's container id:
    ```sh
    docker ps
    ```
5. Then, access the django-app container to initialize our database:
   ```sh
    docker exec -it <container_id> bash
    ```
6. Finally, run the following script:
    ```sh
    sh init-user.sh
    ```
7. To access the web app, open your preferred browser, go to ```http://localhost```


## Project status
WIP
