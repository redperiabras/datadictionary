from django import forms
from django.contrib.auth.models import Group
from dictionary.models import CustomUser

class UserLoginForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['email', 'password']
        widgets = {
            'email': forms.EmailInput(),
            'password': forms.PasswordInput(),
        }

class UserEditForm(forms.ModelForm):
    groups = forms.ChoiceField(choices=[], required=False)
    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'email', 'password', 'groups']
        widgets = {
            'email': forms.EmailInput(),
            'password': forms.PasswordInput(),
        }
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].choices = self.get_group_choices()
        
    def get_group_choices(self):
        return [('', 'Choose Role')] + [(group.id, group.name) for group in Group.objects.all()]
    
class UserCreateForm(forms.ModelForm):
    groups = forms.ChoiceField(choices=[], required=False)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password', 'placeholder': 'Password', 'aria-label': 'password'}))

    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'email', 'groups', 'password']
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control', 'id': 'email', 'placeholder': 'Email Address', 'aria-label': 'email'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'id': 'first_name', 'placeholder': 'First Name', 'aria-label': 'first_name'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'id': 'last_name', 'placeholder': 'Last Name', 'aria-label': 'last_name'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].choices = self.get_group_choices()

    def get_group_choices(self):
        return [('', 'Choose Role')] + [(group.id, group.name) for group in Group.objects.all()]
    
    def save(self, commit=True):
        user = super().save(commit=False)
        password = self.cleaned_data.get('password')
        user.set_password(password)  # Hash the password before saving

        if commit:
            user.save()  # Save the user first to get an ID

            group_id = self.cleaned_data.get('groups')
            if group_id:
                group = Group.objects.get(pk=group_id)
                user.groups.add(group)  # Add the user to the selected group

        return user
    

class FlatFileUploadForm(forms.Form):
    file = forms.FileField()

class RelationalDatabaseForm(forms.Form):
    dbType = forms.ChoiceField(choices=[('mysql', 'MySQL')], required=True)
    dbName = forms.CharField(max_length=30)
    hostname = forms.CharField(max_length=150)
    port = forms.IntegerField()
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())