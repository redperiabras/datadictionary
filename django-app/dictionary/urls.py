from django.urls import path, include
from . import views

urlpatterns = [
    # All User View Routes
    path('', views.user_login, name="login"),
    path('logout/', views.user_logout, name="logout"),
    path('_handle_user/', views.handle_user,  name='handleUser'),
    path('about_cata', views.about_cata, name="aboutCata"),
    
    
    # Admin View Page Routes
    path('admin/manage_users/', views.manage_users, name="manageUsers"),
    path('admin/ingest/database/', views.rdbms_view, name="ingestData"),
    path('admin/ingest/flatfile/', views.process_csv, name="process_csv"),


    # RDBMS
    path('admin/process/rdbms/', views.rdbms_process, name="processRdbms"),
    path('admin/ingest/rdbms/', views.rdbms_ingestion, name="ingestRdbms"),


    # Manage Tables Routes
    path('admin/manage_tables/', views.manage_tables, name="manageTables"),
    path('admin/manage_tables/view/<int:groupId>', views.view_tables, name="viewTables"),
    path('admin/manage_tables/archive/<int:groupId>', views.archived_group, name="archivedGroup"),


    # Annotation Columns Routes API
    path('admin/annotate/<int:groupId>', views.annotation_save, name="annotationColumn"),
    

    # Admin User Action Routes
    path('admin/user/create/', views.create_user, name="createUser"),
    path('admin/user/delete/<int:userId>', views.delete_user, name="deleteUser"),
    path('admin/user/edit/', views.edit_user, name="editUser"),
    path('admin/user/fetch/<int:userId>', views.fetch_user, name="fetchUser"),
    

    #Generate template
    path('generate_template/', views.generate_template, name='generate_template'),

]

