from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

# Custom User Manager model
class CustomUserManager(BaseUserManager):

    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)
    
# Custom User Model
class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    is_visible = models.BooleanField(default=True)  # New field to represent user visibility
    is_active = models.BooleanField(default=True)  # New field to represent user activation


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

class GroupTable(models.Model):
    group_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False)
    schema_num = models.IntegerField(null=False, default=0)
    table_num = models.IntegerField(null=False, default=0)
    column_num = models.IntegerField(null=False, default=0)
    is_archived = models.BooleanField(null=False, blank=False, default=False)

# Table model      
class Table(models.Model):
    table_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False)
    group = models.ForeignKey(GroupTable, on_delete=models.CASCADE, db_column='group_id')

# Column model
class Column(models.Model):
    column_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False)
    sample = models.TextField(blank = True)
    datatype = models.CharField(max_length=255, null=False)
    table = models.ForeignKey(Table, on_delete=models.CASCADE, db_column='table_id')
    
# Annotation model
class Annotation(models.Model):
    annotation_id = models.AutoField(primary_key=True)
    comments = models.TextField(null=True, blank = True)
    timestamp = models.DateTimeField(auto_now_add=True)
    email = models.ForeignKey(CustomUser, on_delete=models.CASCADE, db_column='id')
    column = models.ForeignKey(Column, null=True, on_delete=models.SET_NULL, db_column='column_id')

    class Meta:
        get_latest_by = 'timestamp' 