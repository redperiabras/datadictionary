from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from custom.decorators import groups_required
from django.shortcuts import redirect, render
from django.contrib.auth import logout
from django.http import JsonResponse, HttpResponse
from django.contrib import messages
from django.urls import reverse
from custom import rdbms
from . import models, forms
from custom import user
import pandas as pd
import json


# User Login View
def user_login(request):
    loginForm = forms.UserLoginForm()
    if not request.user.is_authenticated:
        if request.method == "POST":
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse('handleUser'))
        context = {'form': loginForm}
        return render(request, "user_pages/users/login.jinja", context)
    return redirect(reverse('handleUser'))


# User Logout View
def user_logout(request):
     logout(request)
     return redirect(reverse("login"))



# User Role Handle View
def handle_user(request):
    roles = ['admin', 'contributor', 'reader']
    for role in roles:
        if request.user.groups.filter(name__iexact=role).exists():
            request.session['type'] = role.upper()
            if role == "admin":
                return redirect(reverse('manageUsers'))
            elif role == "contributor":
                return redirect(reverse('manageTables'))
            elif role == "reader":
                return redirect(reverse('manageTables'))
    
 
# User Management View
@login_required
@groups_required(['Admin'])
def manage_users(request):
    context = request.session.get('data')
    editForm = forms.UserEditForm()
    createForm = forms.UserCreateForm()
    if request.method == 'POST':
        if createForm.is_valid():
            createdUser = createForm.save(commit=False)
            password = createForm.cleaned_data['password']
            createdUser.set_password(password)
            createdUser.save()

    users = user.fetchAll().order_by('-id')
    context = {'editForm' : editForm, 'createForm': createForm, 'users' : users}
    return render(request, "user_pages/dashboard/manage_users/manage_users.html", context)



# Ingest RDBMS View
@login_required
@groups_required(['Admin'])
def rdbms_view(request):
    rdbmsForm = forms.RelationalDatabaseForm()
    return render(request, 'user_pages/dashboard/ingest_data/ingest_database.html', {'rdbmsForm':rdbmsForm})


# Table Management View
@login_required
@groups_required(['Admin', 'Contributor', 'Reader'])
def manage_tables(request):
     groupTables = models.GroupTable.objects.all().order_by('-group_id')
     return render(request, "user_pages/dashboard/manage_table/manage_table.html", {'groups': groupTables})


#View Tables
@login_required
@groups_required(['Admin', 'Contributor', 'Reader'])
def view_tables(request, groupId):
    group = models.GroupTable.objects.get(group_id=groupId)
    tables = group.table_set.all()
    return render(request, "user_pages/dashboard/manage_table/view_table.html", {'groupId' : group.group_id, 'datasource' : group.name, 'tables' : tables})


# Group Tables Archived
@login_required
@groups_required(['Admin'])
def archived_group(request, groupId):
    group = models.GroupTable.objects.get(group_id=groupId)
    if group.is_archived:
        group.is_archived = False
    else:
        group.is_archived = True
    group.save()
    return JsonResponse(group.is_archived, safe=False)


#About Cata View
@login_required
@groups_required(['Admin', 'Contributor', 'Reader'])
def about_cata(request):
    return render(request, "user_pages/dashboard/about_cata/about_cata.html")

# Fetch Single User API View
@login_required
@groups_required(['Admin'])
def fetch_user(request, userId):
    if request.method == "GET":
        data = user.fetch(userId)
        return JsonResponse(data, safe=False)


# RDBMS Process API View
@csrf_exempt
@login_required
@groups_required(['Admin'])
def rdbms_process(request):
    if request.method == 'POST':
        data = rdbms.rdbms(request.POST)
        return JsonResponse(data, safe=False)
    

# RDBMS Ingestion API View
@login_required
@groups_required(['Admin'])
def rdbms_ingestion(request):
    if request.method == 'POST':
        dbName = request.POST['databaseName']
        schemaNum = request.POST['schemaNum']
        columnNum = request.POST['columnNum']
        data = request.POST.getlist('ingestSelection')
        rdbms.save(dbName, schemaNum, columnNum, data)
        return redirect(reverse('ingestData'))


# Save Column Annotation API View
@login_required
@groups_required(['Admin', 'Contributor'])
def annotation_save(request, groupId):
    if request.method == "POST":
        data = request.POST['annotation']
        userId = request.user.id
        loadData = json.loads(data)
        changedData = []
        for item in loadData:
            colModel =  models.Column.objects.get(column_id=item['id'])
            userModel = models.CustomUser.objects.get(id=userId)
            annotModel = models.Annotation(comments=item['annotate'], column=colModel, email=userModel)
            annotModel.save()
            fullname = f"{userModel.first_name} {userModel.last_name}"
            changedData.append({"id": colModel.column_id, "name": fullname, "email": userModel.email})
    return JsonResponse(changedData, safe=False)


# Create User API View
@login_required
@groups_required(['Admin'])
def create_user(request):
    if request.method == 'POST':
        data = request.POST
        isCreateUser = user.create(data)
    return redirect(reverse('manageUsers'))


# Delete User API View
@login_required
@groups_required(['Admin'])
def delete_user(request, userId):
    isDelete = user.delete(userId)
    if isDelete:
        messages.success(request, "User has been successfully deleted.")
    else:
        messages.error(request, "User does not exist.")
    return redirect("manageUsers")


# Update User Profile API View
@login_required
@groups_required(['Admin'])
def edit_user(request):
    if request.method == "POST":
        data = request.POST
        userId = data.get("id")
        isUpdate = user.update(userId, data)
        return redirect(reverse("manageUsers")) if isUpdate else redirect(reverse("logout"))


# generate Template
@login_required
@groups_required(['Admin'])
def generate_template(request):
    # Create the data structure
    data = [
        {
            "column": "",
            "datatype": "",
            "sample": "",
            "description": ""
        }
    ]
    # Convert data to JSON
    json_data = json.dumps({"data": data}, indent=4)
    # Create a response with the JSON content-type
    response = HttpResponse(json_data, content_type='application/json')
    # Set the Content-Disposition header to trigger a download
    response['Content-Disposition'] = 'attachment; filename="template.json"'
    return response


# Process flatfile upload
@login_required
@groups_required(['Admin'])
def process_csv(request):
    if request.method == 'POST':
        x = 0
        if 'ingest' in request.POST:
            x = 1
            csv_file = request.FILES['csv_file']
            file_name, file_extension = csv_file.name.split('.')
            
            if file_extension.lower() == 'csv':
                num_columns, column_info = process_uploaded_csv(csv_file)
                
                return render(request, 'user_pages/dashboard/ingest_data/ingest_flatfile.html', {
                    'num_columns': num_columns,
                    'column_info': column_info,
                    'csv_file': csv_file.name,
                    'x': x  # Pass x to the template context
                })
            elif file_extension.lower() == 'json':
                num_columns, column_info = process_uploaded_json(csv_file)
                return render(request, 'user_pages/dashboard/ingest_data/ingest_flatfile.html', {
                    'num_columns': num_columns,
                    'column_info': column_info,
                    'csv_file': csv_file.name,
                    'x': x  # Pass x to the template context
                })

            # Render the template with the CSV information

        elif 'save' in request.POST:
            x = 0
            csv_file = request.POST.get('csv_file')
            num_columns = int(request.POST.get('num_columns'))
            column_info = eval(request.POST.get('column_info'))

            # Save the CSV data to the database
            save_csv_data(csv_file, num_columns, column_info)

            return render(request, 'user_pages/dashboard/ingest_data/ingest_flatfile.html')

    return render(request, 'user_pages/dashboard/ingest_data/ingest_flatfile.html')


def process_uploaded_csv(csv_file):
    df = pd.read_csv(csv_file)
    num_columns = df.shape[1]
    column_info = []
    for column_name in df.columns:
        data_type = determine_data_type(df[column_name])
        random_data_point = df[column_name].sample(1).values[0]
        column_info.append({'name': column_name, 'data_type': data_type, 'random_data_point': random_data_point})
    return num_columns, column_info


def process_uploaded_json(json_file):
    df = pd.read_json(json_file)
    num_columns = df.shape[1]
    column_info = []
    for column_name in df.columns:
        data_type = determine_data_type(df[column_name])
        random_data_point = df[column_name].sample(1).values[0]
        column_info.append({'name': column_name, 'data_type': data_type, 'random_data_point': random_data_point})
    return num_columns, column_info

def determine_data_type(column):
    # Check if the column contains any missing values (NaN)
    if column.isnull().any():
        return 'Mixed (includes NaN)'
    
    # Determine the data type based on the non-null values in the column
    unique_values = column.dropna().unique()
    if len(unique_values) == 0:
        return 'Unknown (all values are NaN)'
    
    data_type = str(column.dropna().dtype)
    return data_type



def save_csv_data(csv_file, num_columns, column_info):
    groupTable = models.GroupTable.objects.create(name=csv_file, schema_num=1, table_num=1, column_num=num_columns )
    groupTable.save()

    table = models.Table.objects.create(name=csv_file, group=groupTable)
    table.save()    
    for column in column_info:
        column_table = models.Column.objects.create(name=column['name'], datatype=column['data_type'], table=table, sample=column['random_data_point'])
        column_table.save()
