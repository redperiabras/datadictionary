# Generated by Django 4.2.2 on 2023-07-18 07:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dictionary', '0014_alter_table_group_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='table',
            old_name='group_id',
            new_name='group',
        ),
    ]
