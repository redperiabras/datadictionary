from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models

# Custom Admin User Dashboard
class CustomUserAdmin(UserAdmin):
    model = models.CustomUser
    list_display = ('id', 'email', 'first_name', 'last_name','is_staff', 'is_active',)
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('email', 'first_name', 'last_name','password')}),
        ('Permissions', {'fields': ('is_staff', 'is_visible', 'is_active', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'password1', 'password2', 'is_staff', 'is_visible', 'is_active', 'groups')}
        ),
    )
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


# Register your models here.
admin.site.register(models.CustomUser, CustomUserAdmin)
admin.site.register(models.Annotation)
admin.site.register(models.Column)
admin.site.register(models.Table)
admin.site.register(models.GroupTable)