from django.contrib.auth.models import Group
from dictionary import models, forms
import json

def fetchAll():
    return models.CustomUser.objects.all()

def fetch(userId):
    user = models.CustomUser.objects.get(id=userId)
    group = user.groups.first()
    data = {
        'email' : user.email, 
        'first_name' : user.first_name, 
        'last_name' : user.last_name, 
        'role' : group.id if group else None
    }
    return json.dumps(data)

def create(data):
    form = forms.UserCreateForm(data)
    if form.is_valid():
        form.save()
        return True
    return False

def update(userId, editData):
    user = models.CustomUser.objects.get(id=userId)
    form = forms.UserEditForm(editData, instance=user)
    if form.is_valid():
        user.set_password(form.cleaned_data['password'])
        group = Group.objects.get(id=int(editData.get("groups")))
        user.groups.clear()
        user.groups.add(group)
        user.save()
        return True
    return False

def delete(userId):
    try:
        user = models.CustomUser.objects.get(id=userId)
        user.is_visible = False  # Update the user's visibility to False
        user.is_active = False  # Mark the user as inactive
        user.save()
        return True
    except models.CustomUser.DoesNotExist:
        return False