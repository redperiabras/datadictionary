from django.shortcuts import redirect
from django.urls import reverse

def groups_required(groupNames):
    def decorator(view_func):
        def wrapper(request, *args, **kwargs):
            for group in groupNames:
                if request.user.groups.filter(name__iexact =group).exists():
                    return view_func(request, *args, **kwargs)    
            return redirect(reverse('handleUser'))
        return wrapper
    return decorator