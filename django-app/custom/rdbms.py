import sqlalchemy as db
from dictionary import forms, models
import json

# Connection String Builder for RDBMS
def connectionStringBuilder(dbType, host, port, dbName, user, passw):
    if dbType == "mysql":
        return f"mysql+mysqlconnector://{user}:{passw}@{host}:{port}/{dbName}"

# RDBM Process
def Rdbmsprocess(dbName, connectionString):
    data = []
    engine = db.create_engine(connectionString)
    connection = engine.connect()
    metadata = db.MetaData(bind=engine)
    metadata.reflect()
    
    num_schema_query = connection.execute("SELECT COUNT(*) FROM information_schema.schemata")
    tables = metadata.tables
    for table in tables:
        x,y = {}, {}
        table = tables[table]
        columns = table.columns
        for column in columns:
            result = connection.execute(f"SELECT {column} FROM {table} ORDER BY RAND() LIMIT 2")
            sampData = [row[0] for row in result.fetchall()]
            y[f"{column.name}"] = [str(column.type), sampData]
        x[f"{table}"] = y
        data.append(x)
    engine.dispose()
    return {'name': dbName, 'schema': num_schema_query.scalar(), 'data' : data}

# Complete Process of Fetching RDBMS data
def rdbms(data):
    rdbmsData = forms.RelationalDatabaseForm(data)
    if rdbmsData.is_valid():
        connectionString = connectionStringBuilder(dbType=rdbmsData.cleaned_data['dbType'], host=rdbmsData.cleaned_data['hostname'], \
                                                    port=str(rdbmsData.cleaned_data['port']), dbName=rdbmsData.cleaned_data['dbName'], \
                                                    user=rdbmsData.cleaned_data['username'], passw=rdbmsData.cleaned_data['password'])
        data = Rdbmsprocess(dbName=rdbmsData.cleaned_data['dbName'], connectionString=connectionString)
        return data
    return False

# Ingest RDBMS
def save(dbName, schemaNum, columnNum, data):
    x = f"[{', '.join(data)}]"
    loadData =  json.loads(x)
    group = models.GroupTable(name= dbName, schema_num=schemaNum, table_num=len(loadData), column_num=columnNum)
    group.save()
    for item in loadData:
        tblModel = models.Table(name=''.join(item.keys()), group=group)
        tblModel.save()
        for value_dict in item.values():
            colName = list(value_dict.keys())
            colType = list(item[0] for item in list(value_dict.values()))
            colSample = list(item[1] for item in list(value_dict.values()))
            for i in range(len(colName)):
                sample = ", ".join(map(str, colSample[i]))
                colModel = models.Column(name=colName[i], sample=sample, datatype=colType[i], table=tblModel)
                colModel.save()
