
function fetchUserData(userId){
    $.ajax({
        url: 'http://localhost/admin/user/fetch/'+userId,
        method: 'get',
        success: function(response){
            data = JSON.parse(response);
            $('#editId').val(userId);
            $('#editEmail').val(data["email"]);
            $('#editFname').val(data["first_name"]);
            $('#editLname').val(data["last_name"]);
            $('#editRole').val(data["role"]);
        }
    })
}

function rdbmsProcess(){
    var dbType = $('#databaseType').val()
    var hostname = $('#databaseHostname').val()
    var dbName = $('#databaseName').val()
    var dbPort = $('#databasePort').val()
    var dbUser = $('#databaseUsername').val()
    var dbPass = $('#databasePassword').val()
    $.ajax({
        url: 'http://localhost/admin/process/rdbms/',
        method: 'post',
        data: {
            dbType : dbType,
            dbName : dbName,
            hostname : hostname,
            port : dbPort,
            username : dbUser,
            password : dbPass
        },
        beforeSend: function(){
            $('#loaderModal').modal('show');
        },
        success: function(response){
            var data = JSON.parse(JSON.stringify(response));
            var col_sum = 0
            var ingestHtml = "";
            $('#schemaDisplay').text(data['schema'] + " Schema");
            $('#tableDisplay').text(data['data'].length + " Tables");
            for(var i=0; i<data['data'].length; i++){
                col_sum += Object.keys(Object.values(data['data'][i])[0]).length;
                ingestHtml += "<tr><td><input type=\"checkbox\" name=\"ingestSelection\" class=\"checkbox\" value='"+JSON.stringify(data['data'][i])+"'></td><td>"+Object.keys(data['data'][i])[0]+"</td><td style=\"text-align: center;\">"+Object.keys(Object.values(data['data'][i])[0]).length+"</td></tr>";
            }
            $('#columnDisplay').text(col_sum + " Columns");
            //value
            $('#IngestSelectionDisplay').html(ingestHtml);
            $('#databaseNameData').val(data['name']);
            $('#schemaNumData').val(data['schema']);
            $('#columnNumData').val(col_sum);
            //show
            $('#foundCard').show();
            $('#loaderModal').modal('hide');
        },
        error: function(error) {
            $('#loaderModal').modal('hide');
            $('#errorModal').modal('show');
        }
    });
}


function openModal() {
    $('#viewTableModal').modal('show');
}

function toggleCheckboxes() {
    var checkboxes = document.getElementsByClassName('checkbox');
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = document.getElementById('checkAll').checked;
    }
}

function selectTables(event) {
    event.preventDefault(); // Prevent the default form submission
    
    var checkboxes = document.getElementsByClassName('checkbox');
    var selectedTables = [];
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            selectedTables.push(checkboxes[i].parentNode.nextElementSibling.textContent.trim());
        }
    }
    //document.getElementById('selectedTables').textContent = selectedTables.length + ' Tables selected: ' + selectedTables.join(', ');
    document.getElementById('selectedTables').textContent = selectedTables.length + ' Table(s) selected ';
    $('#viewTableModal').modal('hide');
}
